<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'agenda_evenementiel' => 'Agenda évènementiel',
	'sans_lien_label_case' => 'Ne pas mettre de liens vers les évènements',
	'couleur_label' => 'Couleur des évènements',
	'couleur_defaut_label' => 'Par défaut',
	'couleur_evenement_label' => 'Associé à l\'évènement',
	'couleur_article_label' => 'Associé à l\'article de l\'évènement',
	'couleur_rubrique_label' => 'Associé à la rubrique de l\'évènement',
	'couleur_mot_label' => 'Associé au mot de l\'évènement',
);

