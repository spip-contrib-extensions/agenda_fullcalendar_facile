# Changelog

## 3.1.0 - 2023-12-14

### Added

- Déclaration du modèle pour `inserer_modeles`
- #13 Option `sans_lien` à passer au modèle pour ne pas avoir de lien
### Fixed

- #11 Utiliser l'URL de l'évènement si les évènements ont une page individuelle, aussi pour l'affichage hors JS

## 3.0.2 - 2023-11-17

### Added

- #12 Compatibilité SPIP 4.2

### Fixed

- #11 Utiliser l'URL de l'évènement si les évènements ont une page individuelle
